# Species Distribution Modelling Workflow in R

__Main author:__  Jessica Nephin   
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
  + [Software](#software)
  + [Input Data](#input-data)
  + [Directory Structure](#directory-structure)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
An automated workflow for generating reproducible species distribution model predictions and uncertainty estimates for multiple species.

## Summary
Species distribution models (SDMs) have become increasingly valuable as a tool for the management and conservation of marine resources and places. The SDM framework (Nephin et al. 2020), implemented with R scripts found in this project, were developed to produce consistent, interpretable, and defensible SDMs to support DFO’s contribution to Canada's ocean policies. The R workflow contains data preparation, cross-validation, model fitting and evaluation, prediction, uncertainty estimation and model interpretation components. Currently, generalized linear models (GLM), generalized additive models (GAM), boosted regression tree (BRT) and maximum entropy (Maxent) fitting methods are implemented. The workflow can be repeated multiple times with different model fitting methods to create an ensemble model prediction. Multi-model ensembles produce robust model predictions and provide insight into model uncertainty.

For the exact version of the framework described in Nephin et al. (2020) checkout the csas branch. The master and develop branch include updates to the SDM workflow that have been made since that publication.

## Status
Ongoing-improvements

## Contents
All scripts described below run from the ~/Scripts working directory. Other files in directory are sourced by the scripts described below.

**DataPrep.R**    
  *  Processes the observations and predictor data prior to modelling
  *  Before executing, adjust 'Control' options within DataPrep.R
  *  Output will be placed in a new directory Projects/*project-name*/DataPrep/

**SDM.R**    
  *  Fits, evaluates and predicts (GLM, GAM, BRT or Maxent)
  *  Before executing, adjust 'Control' and 'Parameter' options within SDM.R
  *  Output will be place in a new directory Projects/*project-name*/Models/*model-type*/

**Ensemble-Evaluate.R**    
  *  Builds an ensemble model from models developed from SDM.R (e.g. GLM, GAM, BRT, Maxent)
  *  Evaluates models not build with SDM.R (e.g. knowledge-based model) using data prepped with DataPrep.R
  *  Calculates the difference between the ensemble and a knowledge-based model (e.g. HSI) if available
  *  Before executing, adjust 'Control' options within Ensemble-Evaluate.R
  *  Output will be place in a new directory Projects/*project-name*/Models/*model-type*/


## Methods

**Data Preparation**    
Several data preparation steps are completed:
* Occurrence data are projected to match the geographic projection of the predictors.
* Occurrence data are then checked for any erroneous (e.g., presence-absence values outside of 0 or 1) or missing values, which are removed.
* Predictor values are extracted from raster layers at the location of the observations, except for predictor variables that are selected to be sourced from the observations dataset.
* The process for extracting predictor values from raster layers varies slightly for point and line species observations. For point data, if more than one observation falls within a predictor raster cell, the points are aggregated to the raster grid (if the aggregation option is selected). Warning: aggregation does not work for point data when categorical terms are sourced from species observation data (e.g. survey type). For observations represented as lines, mean values are calculated for each raster cell that intersects the line segments.
* Missing values in the predictor dataset, which can occur when observations fall outside the extent of any of the raster layers, are removed.
* The predictors are examined for model suitability by comparing the distribution of all values in the raster layers to the distribution of extracted predictor values.
* Variance inflation factor (VIF) and Spearman’s correlation are used to assess collinearity among the extracted predictor variables.

**Cross-validation**    
After the data model has been prepared, it is partitioned into training and testing datasets, used for model fitting and validation, respectively. Spatial block cross-validation approach is used to increase the independence of observations used to fit (training data) and evaluate (testing data) the models. The number of CV folds, the size of spatial blocks and the method to assign folds to spatial blocks can be selected in the DataPrep.R.

**Model Fitting**    
Model selection methods depend on the type of model fitting algorithm used.
*  GLM selection is automated with the cv.glmnet function from the glmnet package which performs model fitting via a penalized maximum likelihood and model selection by using holdout data in a k-fold CV procedure to tune the regularization parameter lambda. GLM options for quadratic and interaction terms can be set in SDM.R.
* GAMs are fit with the mgcv package using double penalization (range space and null space via the select=TRUE option) for model selection, penalizing terms to zero to effectively drop them from the model. GAM options for smooths, smoothing parameter estimation method, basis dimensions, smoothing penalty and interaction terms can be set in SDM.R script.
*  BRT models are built with functions from gbm and dismo R packages. The initial tuning parameters can be set in the SDM.R script. Within the model tuning procedure the training data is split using random CV into internal training and holdout data to evaluate the stopping criterion that determines when the optimal number of trees has been reached.
* Maxent models are built from presence and absence data using the maxnet package, which uses glmnet for model fitting. Maxnet takes advantage of the equivalence between infinitely‐weighted logistic regression and maximum entropy models, and produces models equivalent to Maxent models build from the original MaxEnt Java application (https://biodiversityinformatics.amnh.org/open_source/maxent/). Models are fit using the same feature classes (linear, quadratic, product, threshold, hinge) and lasso regularization as the Java version. Predictions are derived from complementary log-log (cloglog) transformation to produce an estimate of occurrence probability.

**Model Evaluation**    
SDMs can be evaluated for model fit and predictive performance. Model fit is assessed by measuring the variance remaining in the data used to build the model, while predictive performance is measured by examining the agreement between model predictions and observations not used in model fitting. A variety of threshold-independent (e.g. AUC and Tjur's pseudo-Rsquared) and threshold-dependent (e.g. True Skill Statistic and Kappa) metrics are applied. If a knowledge-based prediction has been derived (e.g. expert or threshold based envelop model) based on the known relationships between a species and environmental conditions, it can be evaluated and then compared to the data-driven ensemble model prediction (see Ensemble-Evaluate.R). Code for creating expert-derived models is not included in this project. 

**Prediction**    
Model fitting is completed k-times, once for each CV folds, to create multiple model predictions for each model fitting method (e.g GAM). If a fixed effect is included in the model that has no raster layer to predict with (e.g. survey type), predictions will be produced for each level of the categorical variable (e.g. trap, trawl, longline). In addition, if multiple fitting methods are used (e.g. GLM, GAM, BRT), predictions can then be combined into an ensemble prediction. Predictions can be combined into an ensemble using a weighted mean with AUC for weighting. 

**Uncertainty estimation**    
Uncertainty in the model predictions was assessed with four different methods that reduce or measure uncertainty spatially across a study area.
1.  Uncertainty is reduced by limiting extrapolation.
2.  Uncertainty is evaluated by measuring the variation among model predictions that make up the ensemble.
3.  Uncertainty can evaluated by comparing a knowledge-based model prediction to a data-driven model prediction.
4.  Uncertainty within individuals models is visualized by mapping model residuals.

**Model Interpretation**    
Models are interpreted by examining the relative influence of predictor variables in the model and the marginal effects of each predictor on the observations. The relative influence of predictors for all models except BRT models are estimated using a randomization and permutation method based on the SDMtune R package and the Maxent java application. Marginal effects (or response curves) are estimated using the evaluation strip method detailed in Elith et al. 2005 (https://doi.org/10.1016/j.ecolmodel.2004.12.007). 


## Requirements

### Software
* R version 4.0.2 or newer (https://www.r-project.org/)
* BlockCV package version 2.1.3 or newer (https://github.com/rvalavi/blockCV)
* For full list of required R packages see SDM.R UsePackages()

### Input Data
**Observations**    
*  A single shapefile with point or line data
*  Located in Projects/*project-name*/Data/SpeciesData/ directory
*  Will be projected to the CRS of the environmental data

**Predictors**    
*  Categorical data can be represented as characters or numerical values
*  Accepts a stack of raster data layers (.tif or .asc formats) located in Projects/*project-name*/Data/EnvData directory


### Directory Structure
*  If the name is within <>, it can be changed
*  Create new project folder for each new modelling project

```
/---<SDM>   
    /---Scripts   
    |   
    /---Documentation    
    |         
    /---Projects   
        /---<project-name>   
            /---Coastline   
            |       <polygon-name>.shp   
            |          
            /---Data   
                /---EnvData                
                |       <raster1-name>.tif   
                |       <raster2-name>.tif   
                |           
                /---SpeciesData   
                        <obs-name>.shp   
```

## Caveats
*  This workflow was developed for presence-absence data, but may be adapted for use with density or count data.
*  The use of polygon predictor layers is not supported, only raster files are accepted.

## Uncertainty
Sources of uncertainty arise from methodological choices, and the availability and quality of data. All species observations should be reviewed for positional accuracy and data quality prior to running through this workflow. Uncertainty stemming from a lack of spatial precision in the observations, errors in the predictor layers, missing predictors, or scale mismatches between the predictors and observations are not addressed here.

## Acknowledgements
Cole Fields, Ashley Park, Ed Gregr, Candice St. Germain, Sarah Davies, Matthew H. Grinnell and Jessica Finney

## References
Nephin, J., Gregr, E.J., St. Germain, C., Fields, C., and Finney, J.L. 2020. Development of a Species Distribution Modelling Framework and its Application to Twelve Species on Canada’s Pacific Coast. DFO Can. Sci. Advis. Sec. Res. Doc. 2020/004. xii + 107 p. https://www.dfo-mpo.gc.ca/csas-sccs/Publications/ResDocs-DocRech/2020/2020_004-eng.html
